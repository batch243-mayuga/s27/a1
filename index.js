// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}

{
    "id": 2,
    "firstName": "Gabby",
    "lastName": "Mayuga",
    "email": "gabbymayuga77@gmail.com",
    "password": "gabby1234",
    "isAdmin": false,
    "mobileNo": "09126917111"
}


// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}

{
    "id": 20,
    "userId": 2,
    "productID" : 50,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 2000
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
}

{
    "id": 50,
    "name": "Aircon",
    "description": "Make your home cool at any time.",
    "price": 30000,
    "stocks": 150,
    "isActive": true,
}